#!/usr/bin/env bash
set -euo pipefail

jaesg src
wget -O build/periodic-table.json https://raw.githubusercontent.com/Bowserinator/Periodic-Table-JSON/master/periodic-table-lookup.json

if [ "${1-}" == "serve" ]; then
    python -m http.server --directory build
fi
