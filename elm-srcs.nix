{

      "elm/json" = {
        sha256 = "0kjwrz195z84kwywaxhhlnpl3p251qlbm5iz6byd6jky2crmyqyh";
        version = "1.1.3";
      };

      "elm/html" = {
        sha256 = "1n3gpzmpqqdsldys4ipgyl1zacn0kbpc3g4v3hdpiyfjlgh8bf3k";
        version = "1.0.0";
      };

      "elm/browser" = {
        sha256 = "0nagb9ajacxbbg985r4k9h0jadqpp0gp84nm94kcgbr5sf8i9x13";
        version = "1.0.2";
      };

      "elm/core" = {
        sha256 = "19w0iisdd66ywjayyga4kv2p1v9rxzqjaxhckp8ni6n8i0fb2dvf";
        version = "1.0.5";
      };

      "elm-community/list-extra" = {
        sha256 = "1rvr1c8cfb3dwf3li17l9ziax6d1fshkliasspnw6rviva38lw34";
        version = "8.2.4";
      };

      "chain-partners/elm-bignum" = {
        sha256 = "17fqqmm83r905zw03gjc0dif3ach4d1aar3cv7dlcn2szp0qpgfs";
        version = "1.0.1";
      };

      "elm/regex" = {
        sha256 = "0lijsp50w7n1n57mjg6clpn9phly8vvs07h0qh2rqcs0f1jqvsa2";
        version = "1.0.0";
      };

      "elm/url" = {
        sha256 = "0av8x5syid40sgpl5vd7pry2rq0q4pga28b4yykn9gd9v12rs3l4";
        version = "1.0.0";
      };

      "elm/time" = {
        sha256 = "0vch7i86vn0x8b850w1p69vplll1bnbkp8s383z7pinyg94cm2z1";
        version = "1.0.0";
      };

      "elm/virtual-dom" = {
        sha256 = "0q1v5gi4g336bzz1lgwpn5b1639lrn63d8y6k6pimcyismp2i1yg";
        version = "1.0.2";
      };
}
