{
  description = "My personal site";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable"; # for some reason the default is a local path
    jaesg.url = "git+https://gitlab.com/jD91mZM2/jaesg.git";
    periodicTable = {
      url = "github:Bowserinator/Periodic-Table-JSON";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, jaesg, periodicTable }: let
    forAllSystems = nixpkgs.lib.genAttrs [ "x86_64-linux" "x86_64-darwin" "i686-linux" "aarch64-linux" ];
  in {
    packages = forAllSystems (system: let
      pkgs = nixpkgs.legacyPackages."${system}";
      jaesgExe = jaesg.packages."${system}".jaesg;
    in {
      build = pkgs.stdenv.mkDerivation {
        name = "pages";

        src = ./.;

        phases = [ "unpackPhase" "buildPhase" ];

        buildPhase = ''
          ${pkgs.elmPackages.fetchElmDeps {
            elmPackages = import ./elm-srcs.nix;
            elmVersion = "0.19.1";
            registryDat = ./registry.dat;
          }}

          ${jaesgExe}/bin/jaesg src "$out" --prod
          cp ${periodicTable}/periodic-table-lookup.json "$out/periodic-table.json"
        '';
      };
    });
    defaultPackage = forAllSystems (system: self.packages."${system}".build);

    devShell = forAllSystems (system: let
      pkgs = nixpkgs.legacyPackages."${system}";
    in pkgs.mkShell {
      buildInputs = [
        jaesg.packages."${system}".jaesg
        pkgs.elm2nix
      ];
    });
  };
}
