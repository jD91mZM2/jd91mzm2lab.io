module Overflow.Page exposing (main)

import Browser
import Html exposing (Attribute, Html, div, input, option, select, text)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput)
import Integer exposing (Integer)



-- MAIN


main =
    Browser.sandbox { init = init, update = update, view = view }



-- MODEL


type alias Model =
    { number : Integer
    , bitsize : Bitsize
    }


type alias Bitsize =
    { bits : Int
    , signed : Bool
    }


init : Model
init =
    { number = Integer.zero
    , bitsize = { bits = 8, signed = True }
    }



-- UPDATE


type Msg
    = Type String
    | Select String


update : Msg -> Model -> Model
update msg model =
    case msg of
        Type newContent ->
            let
                newNumber =
                    if String.isEmpty newContent then
                        Just Integer.zero

                    else
                        Integer.fromString newContent
            in
            case newNumber of
                Just num ->
                    { model | number = num }

                Nothing ->
                    model

        Select bitstring ->
            let
                signed =
                    if String.startsWith "i" bitstring then
                        True

                    else
                        False

                bits =
                    Maybe.withDefault 32
                        (String.toInt (String.dropLeft 1 bitstring))
            in
            { model
                | bitsize =
                    { bits = bits, signed = signed }
            }



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ select [ onInput Select ]
            (List.map (makeOption model.bitsize True) (bitSizes 8 64)
                ++ List.map (makeOption model.bitsize False) (bitSizes 8 64)
            )
        , input [ type_ "number", value (Integer.toString model.number), onInput Type ] []
        , div [] [ text (wrap model.bitsize model.number |> Integer.toString) ]
        ]



-- Make html option for integer


makeOption : Bitsize -> Bool -> Int -> Html a
makeOption current signed size =
    let
        name =
            (if signed then
                "i"

             else
                "u"
            )
                ++ String.fromInt size
    in
    option
        [ value name
        , selected (current.signed == signed && current.bits == size)
        ]
        [ text
            ((if signed then
                "S"

              else
                "Uns"
             )
                ++ "igned "
                ++ String.fromInt size
                ++ "-bit integer"
            )
        ]



-- Get series of powers of two within bounds


bitSizes first last =
    if first > last then
        []

    else
        [ first ] ++ bitSizes (first * 2) last



-- Arbitrary big exponentiation


pow : Integer -> Integer -> Integer
pow x n =
    let
        ( div, mod ) =
            Maybe.withDefault ( Integer.zero, Integer.zero ) (Integer.divmod n (Integer.fromInt 2))
    in
    if n == Integer.zero then
        Integer.one

    else if mod == Integer.zero then
        -- Fast exponention by squaring
        pow (Integer.mul x x) div

    else
        Integer.mul x (pow x (Integer.sub n Integer.one))


pow2 : Int -> Integer
pow2 =
    Integer.fromInt >> pow (Integer.fromInt 2)



-- Get overflow number (2^n where n is number of bits)


overflowNumber bitsize =
    pow2 bitsize.bits



-- Get minimum number for an integer. For unsigned, this is zero.


numberMin : Bitsize -> Integer
numberMin bitsize =
    if not bitsize.signed then
        Integer.zero

    else
        Integer.negate (pow2 (bitsize.bits - 1))


wrap : Bitsize -> Integer -> Integer
wrap bitsize number =
    let
        -- Make the range from 0 <= x < (m-n) instead of n <= x < m
        offset =
            numberMin bitsize

        newNumber =
            Integer.sub number offset

        -- Wrap number
        mod =
            Maybe.withDefault Integer.zero (Integer.remainderBy newNumber (overflowNumber bitsize))
    in
    -- Reset number range
    Integer.add mod offset
