module Chemistry.Page exposing (main)

import Browser
import Chemistry.Eval exposing (compile)
import Chemistry.Parser exposing (parse)
import Dict exposing (Dict)
import Html exposing (Html, button, div, input, p, text)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput)
import Json.Decode as D exposing (Decoder, dict, field, float, int, map4, string)
import Decimal exposing (Decimal)



-- Main & Init


main =
    Browser.element
        { init = init
        , update = update
        , view = view
        , subscriptions = \_ -> Sub.none
        }


type alias Element =
    { name : String
    , atomicMass : Decimal
    , number : Int
    , symbol : String
    }


elementDecoder : Decoder Element
elementDecoder =
    map4 Element
        (field "name" string)
        (D.map Decimal.fromFloat (field "atomic_mass" float))
        (field "number" int)
        (field "symbol" string)


type alias Flags =
    { periodicTable : Dict String Element
    }


flagsDecoder : Decoder Flags
flagsDecoder =
    D.map Flags
        (field "periodicTable" (dict elementDecoder))


init : D.Value -> ( Model, Cmd Msg )
init raw =
    let
        flagsResult =
            D.decodeValue flagsDecoder raw

        flags =
            case flagsResult of
                Ok val ->
                    val

                Err err ->
                    { periodicTable = Dict.empty }
    in
    ( { periodicTable = Dict.values flags.periodicTable
      , input = ""
      }
    , Cmd.none
    )



-- Update


type alias Model =
    { periodicTable : List Element
    , input : String
    }


type Msg
    = Input String


update msg model =
    case msg of
        Input text ->
            ( { model | input = text }, Cmd.none )



-- View


findElementBySymbol : List Element -> String -> Maybe Element
findElementBySymbol elements symbol =
    List.head (List.filter (\elem -> elem.symbol == symbol) elements)


resolveMaybeList : List (Maybe a) -> Maybe (List a)
resolveMaybeList list =
    List.foldl
        (\maybeA acc -> Maybe.andThen (\a -> Maybe.map (\current -> current ++ [ a ]) acc) maybeA)
        (Just [])
        list


resolveCount : List Element -> List ( String, Int ) -> Result String (List ( Element, Int ))
resolveCount periodicTable list =
    Result.fromMaybe "Unknown element"
        (resolveMaybeList
            (List.map
                (\( sym, count ) ->
                    Maybe.map
                        (\elem -> ( elem, count ))
                        (findElementBySymbol periodicTable sym)
                )
                list
            )
        )


parseAndCount : Model -> Result String (List ( Element, Int ))
parseAndCount model =
    Result.andThen (compile >> Dict.toList >> resolveCount model.periodicTable) (parse model.input)


mapToString : String -> (a -> String) -> List a -> String
mapToString sep f list =
    String.join sep (List.map f list)


viewInfo : List ( Element, Int ) -> Html Msg
viewInfo elements =
    div []
        [ p [] [ text ("Names: " ++ mapToString " + " (\( elem, cnt ) -> String.fromInt cnt ++ "x " ++ elem.name) elements) ]
        , p [] [ text ("Numbers: " ++ mapToString " + " (\( elem, cnt ) -> String.fromInt elem.number ++ "p") elements) ]
        , p []
            [ text
                ("Weighs: "
                    ++ mapToString " + " (\( elem, cnt ) -> Decimal.toString (Decimal.mul (Decimal.fromInt cnt) elem.atomicMass)) elements
                    ++ " = "
                    ++ Decimal.toString (List.foldl (\( elem, cnt ) acc -> Decimal.add acc (Decimal.mul (Decimal.fromInt cnt) elem.atomicMass)) (Decimal.fromInt 0) elements)
                )
            ]
        ]


view : Model -> Html Msg
view model =
    let
        elements =
            parseAndCount model

        info =
            case elements of
                Err err ->
                    p [] [ text ("Error: " ++ err) ]

                Ok html ->
                    viewInfo html
    in
    div []
        [ input [ type_ "text", onInput Input ] []
        , info
        ]
