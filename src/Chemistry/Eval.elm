module Chemistry.Eval exposing (compile)

import Chemistry.Parser exposing (AST(..))
import Dict exposing (Dict)

type alias Count = Dict String Int


updateCount k v dict =
    if Dict.member k dict then
        Dict.update k (Maybe.map ((+) v)) dict

    else
        Dict.insert k v dict


updateBoth k v1 v2 dict =
    updateCount k v1
        (updateCount k v2 dict)


merge : Count -> Count -> Count
merge lhs rhs =
    Dict.merge
        -- only left
        updateCount
        -- both
        updateBoth
        -- only right
        updateCount
        -- dictionary #1
        lhs
        -- dictionary #2
        rhs
        -- result to be filled
        Dict.empty


mul : Int -> Count -> Count
mul constant dict =
    Dict.map (\_ -> (*) constant) dict


compile : AST -> Dict String Int
compile ast =
    case ast of
        Compound name amount ->
            Dict.singleton name amount

        Group compounds amount ->
            let
                compiled =
                    List.map compile compounds

                merged =
                    List.foldl merge Dict.empty compiled
            in
            mul amount merged
