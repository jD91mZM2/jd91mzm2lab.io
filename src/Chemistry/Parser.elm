module Chemistry.Parser exposing (AST(..), parse)

import List.Extra exposing (span)



-- Lexer


type Token
    = ParenOpen
    | ParenClose
    | Element String
    | Repeat Int

showToken : Token -> String
showToken token =
    case token of
        ParenOpen -> "ParenOpen"
        ParenClose -> "ParenClose"
        Element text -> "Element " ++ text
        Repeat n -> "Repeat " ++ String.fromInt n

showTokens : List Token -> String
showTokens list = "[" ++ String.join ", " (List.map showToken list) ++ "]"

isUpperAlpha c =
    c >= 'A' && c <= 'Z'


isLowerAlpha c =
    c >= 'a' && c <= 'z'


isNumeric c =
    c >= '0' && c <= '9'


tokenize : List Char -> Result String (List Token)
tokenize str =
    case str of
        [] ->
            Ok []

        '(' :: xs ->
            Result.map (\rest -> ParenOpen :: rest) (tokenize xs)

        ')' :: xs ->
            Result.map (\rest -> ParenClose :: rest) (tokenize xs)

        first :: xs ->
            if isUpperAlpha first then
                let
                    ( head, tail ) =
                        span isLowerAlpha xs
                in
                Result.map
                    (\rest -> Element (String.fromList (first :: head)) :: rest)
                    (tokenize tail)

            else if isNumeric first then
                let
                    ( head, tail ) =
                        span isNumeric xs

                    numeric =
                        String.fromList (first :: head)

                    number =
                        Result.fromMaybe "invalid number" (String.toInt numeric)
                in
                Result.andThen
                    (\num -> Result.map (\rest -> Repeat num :: rest) (tokenize tail))
                    number

            else
                Err "unknown token"



-- Parser


type AST
    = Compound String Int
    | Group (List AST) Int


parseGroup : List Token -> Result String ( List AST, List Token )
parseGroup tokens =
    case tokens of
        ParenClose :: xs ->
            Ok ( [], tokens )

        [] ->
            Ok ( [], [] )

        _ ->
            case parseCompound tokens of
                Err err ->
                    Err err

                Ok ( compound, rest ) ->
                    case parseGroup rest of
                        Err err ->
                            Err err

                        Ok ( compounds, xs ) ->
                            Ok ( compound :: compounds, xs )


parseCompound : List Token -> Result String ( AST, List Token )
parseCompound tokens =
    case tokens of
        (Element elem) :: (Repeat num) :: xs ->
            Ok ( Compound elem num, xs )

        (Element elem) :: xs ->
            Ok ( Compound elem 1, xs )

        ParenOpen :: inner ->
            case parseGroup inner of
                Err err ->
                    Err err

                Ok ( compounds, rest ) ->
                    case rest of
                        ParenClose :: (Repeat num) :: xs ->
                            Ok ( Group compounds num, xs )

                        ParenClose :: xs ->
                            Ok ( Group compounds 1, xs )

                        xs ->
                            Err ("expected closing parenthesis, found " ++ showTokens xs)

        _ ->
            Err ("expected compound, got " ++ showTokens tokens)


parseRoot : List Token -> Result String AST
parseRoot tokens =
    case parseGroup tokens of
        Err err ->
            Err err

        Ok ( ast, xs ) ->
            if List.isEmpty xs then
                Ok (Group ast 1)

            else
                Err ("trailing tokens: " ++ showTokens xs)



-- Parse and tokenize


parse : String -> Result String AST
parse input =
    Result.andThen parseRoot (tokenize (String.toList input))
