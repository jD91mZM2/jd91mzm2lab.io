module Main.Page exposing (main)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput)


main =
    Browser.sandbox { init = init, update = update, view = view }



-- MODEL


init : ()
init =
    ()



-- UPDATE


update : () -> () -> ()
update msg model =
    ()



-- VIEW


view : () -> Html ()
view model =
    ul []
        [ makeLink "Integer overflow calculator" "Overflow.html"
        , makeLink "Chemistry tool" "Chemistry.html"
        ]


makeLink : String -> String -> Html a
makeLink body link =
    li [] [ a [ href link ] [ text body ] ]
